import { Component } from '@angular/core';

@Component({
  selector: 'app-create-registration',
  templateUrl: './create-registration.component.html',
  styleUrls: ['./create-registration.component.css']
})
export class CreateRegistrationComponent {
  public packages = ["Yes" , "No"]
  public gender = ["Female" , "Male" , "Third Gender" , "Others"]
  public dates = ["3" , "6" ,"9" , "12" ,"15" , "18" ,"21" , "24" , "27" , "30" , "33" ,"36"]
}
